import java.io.*;

public class sortedTextFile {

	private String nameOfFile;
	public static final String TMP = "temporal";
	private int numElements;
	private File f;
	private File creator;

	public sortedTextFile(String nameOfFile) throws IOException {
		this.nameOfFile = nameOfFile;
		f = new File(TMP);
		creator = new File(nameOfFile);
		if (!creator.exists()) {
			creator.createNewFile();
		}
		numElements = countElements();
	}

	public String getName() {
		return nameOfFile;
	}

	public void put(String text) throws IOException {

		BufferedReader in = new BufferedReader(new FileReader(nameOfFile));
		PrintWriter out = new PrintWriter(new FileWriter(f));
		boolean textWrite = false;
		String s1;

		try {
			while ((s1 = in.readLine()) != null) {
				if (text.compareTo(s1) < 0 && !textWrite) {
					out.println(text);
					textWrite = true;
					numElements++;
				}
				out.println(s1);

			}
			if (!textWrite) {
				out.println(text);
				numElements++;
			}
			f.renameTo(new File(nameOfFile));
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

	public int getNumElements() {
		return numElements;
	}

	public String getElementAt(int line) throws IOException {

		BufferedReader in = new BufferedReader(new FileReader(nameOfFile));
		int linesCounts = 0;
		String s1 = null;

		try {
			while (linesCounts != line) {
				s1 = in.readLine();
				linesCounts++;
			}
			return s1;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public void deleteElementAt(int line) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameOfFile));
		PrintWriter out = new PrintWriter(new FileWriter(f));
		String s = null;
		int lineNumber = 1;
		try {
			while ((s = in.readLine()) != null) {
				if (lineNumber != line) {
					out.println(s);
				}
				lineNumber++;
			}
			numElements--;
			f.renameTo(new File(nameOfFile));
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

	public boolean isEmpty() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameOfFile));
		try {
			return !(in.readLine() != null);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public void emptiesFile() {
		f.renameTo(new File(nameOfFile));
	}

	public void print() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameOfFile));
		String s = null;
		try {
			while ((s = in.readLine()) != null) {
				System.out.println(s);
			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public boolean existsElement(String sentence) throws IOException {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(nameOfFile));
			String line = "";
			while ((line = in.readLine()) != null) {
				if (line.equalsIgnoreCase(sentence)) {
					return true;
				}
			}
			return false;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	public static void merge(String name1, String name2, String exitFile) throws IOException {
		BufferedReader in = null;
		BufferedReader in2 = null;
		PrintWriter out = null;
		String line1;
		String line2;

		File f = new File(name1);
		File f2 = new File(name2);
		File f3 = new File(exitFile);
		if (!f.exists()) {
			f.createNewFile();
		}
		if (!f2.exists()) {
			f2.createNewFile();
		}
		if (!f3.exists()) {
			f3.createNewFile();
		}
		try {
			in = new BufferedReader(new FileReader(name1));
			in2 = new BufferedReader(new FileReader(name2));
			out = new PrintWriter(new FileWriter(exitFile));
			line1 = in.readLine();
			line2 = in2.readLine();

			while (line1 != null || line2 != null) {
				if (line1 == null) {
					out.println(line2);
					line2 = in2.readLine();
				} else {
					if (line2 == null) {
						out.println(line1);
						line1 = in.readLine();
					} else {
						if (line1.compareTo(line2) < 0) {
							out.println(line1);
							line1 = in.readLine();
						} else {
							out.println(line2);
							line2 = in.readLine();
						}
					}
				}
			}

		} finally {
			if (in != null) {
				in.close();
			}
			if (in2 != null) {
				in2.close();
			}
			if (out != null) {
				out.close();
			}
		}

	}

	private int countElements() throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(nameOfFile));
		int linesCounts = 0;
		try {
			while (in.readLine() != null) {
				linesCounts++;
			}
			return linesCounts;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}
}
