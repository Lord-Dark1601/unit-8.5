import java.io.*;

public class sortedTextFileMain {

	public static void main(String[] args) throws IOException {

		sortedTextFile stf = new sortedTextFile("FileProve");
		sortedTextFile stf2 = new sortedTextFile("FileProve2");

		System.out.println(stf.getName());
		stf.put("I'm the best?");
		stf.put("Obviously, boy. You're!!");

		System.out.println(stf2.getName());
		stf2.put("d");
		stf2.put("e");
		stf2.put("b");
		stf2.put("a");
		stf2.put("c");
		stf2.put("f");

		System.out.println(stf2.getElementAt(3));

	}

}
