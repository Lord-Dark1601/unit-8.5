import javax.swing.*;

public class GraphiExample {

	public static void main(String[] args) {

		int num1 = Integer.parseInt(
				JOptionPane.showInputDialog(null, "Enter first number", "Adding", JOptionPane.QUESTION_MESSAGE));
		int num2 = Integer.parseInt(
				JOptionPane.showInputDialog(null, "Enter second number", "Adding", JOptionPane.QUESTION_MESSAGE));

		int result = num1 + num2;

		JOptionPane.showMessageDialog(null, "Total is: " + result, "Adding", JOptionPane.INFORMATION_MESSAGE);

	}

}
