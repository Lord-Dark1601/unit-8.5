
public class SortedArrayMain {

	public static void main(String[] args) {
		SortedArray sa = new SortedArray(1000000, true);

		int x = (int) (Math.random() * 500000 + 500000);
		System.out.println(x);
		System.out.println(sa.getElementAt(x));
		System.out.println(sa.existElement(x));
		System.out.println(sa.existElementBinary(x));

	}

}
