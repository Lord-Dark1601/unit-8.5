
public class SortedArray {

	private int[] array = null;
	private int numElements;

	public SortedArray(int i) {
		numElements = 0;
		array = new int[i];
		for (int j = 0; j < array.length; j++) {
			array[j] = -1;
		}
	}

	public SortedArray(int i, boolean prove) {
		if (prove) {
			numElements = i;
			array = new int[i];
			for (int j = 0; j < array.length; j++) {
				array[j] = j;
			}
		}
	}

	public void put(int number) {
		int n = number;
		int n2 = -1;
		numElements++;
		int i = 0;
		while (number <= array[i] || array[i] == -1) {
			if (array[i] == -1) {
				array[i] = n;
				break;
			} else {
				if (n < array[i]) {
					n2 = array[i];
					array[i] = n;
					n = n2;
				}
			}
			i++;
		}
	}

	public void putProfe(int n) {
		int i = 0;
		while (i < numElements && array[i] < n) {
			i++;
		}
		// i has now the position in which to insert
		for (int j = numElements; j > i; j--) {
			if (j < array.length) {
				array[j] = array[j - 1];
			}
		}
		if (i < array.length) {
			array[i] = n;
		}
		if (numElements < array.length) {
			numElements++;
		}
	}

	@Override
	public String toString() {
		String s = "";
		boolean first = true;
		int i = 0;
		while (array[i] != -1) {
			if (first) {
				s += array[i];
				first = false;
			} else {
				s += ", " + array[i];
			}
			i++;
		}

		return s;
	}

	public int returnElements() {
		return numElements;
	}

	public int getSize() {
		return array.length;
	}

	public int getElementAt(int position) {
		return array[position];
	}

	public void removeElementAt(int position) {
		for (int i = position; i < numElements - 1; i++) {
			array[i] = array[i + 1];
		}
		array[numElements - 1] = -1;
		numElements--;
	}

	public boolean isEmpty() {
		return (array[0] != -1);
	}

	public boolean isFull() {
		return (array[getSize() - 1] != -1);
	}

	public boolean existElement(int number) {
		int i = 0;
		while (i < numElements && array[i] < number) {
			i++;
		}
		if (i < numElements && array[i] == number) {
			return true;
		}
		return false;
	}

	public boolean existElementBinary(int element) {
		return binarySearch(element, 0, numElements - 1);
	}

	public boolean binarySearch(int element, int minorIndex, int upperIndex) {
		int middle = ((minorIndex + upperIndex) / 2);
		if (minorIndex > upperIndex) {
			return false;
		} else if (array[middle] == element) {
			return true;
		} else {
			if (element < array[middle]) {
				return binarySearch(element, minorIndex, middle - 1);
			} else {
				return binarySearch(element, middle + 1, upperIndex);
			}
		}
	}

}
