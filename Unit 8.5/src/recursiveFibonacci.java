
public class recursiveFibonacci {

	public static void main(String[] args) {

		System.out.println(fibonacci(8));
	}

	public static long fibonacci(long n) {
		if(n<3) {
			return 1;
		}else {
			return fibonacci(n-1) + fibonacci(n-2);
		}
	}
}
